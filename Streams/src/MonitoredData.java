import java.time.LocalDateTime;

public class MonitoredData {
	public LocalDateTime startTime;
	public LocalDateTime endTime;
	public String activityLabel;
	
	public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activityLabel) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}
	
	public String toString() {
		return "startTime: " + startTime.toString() + ", endTime: " + endTime.toString() + ", label: " + activityLabel;
	}
}
