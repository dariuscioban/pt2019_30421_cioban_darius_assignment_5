import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataProcessing {

	private List<MonitoredData> data;
	private String inputFileName;
	
	public DataProcessing(String inputFileName) {
		this.inputFileName = inputFileName + ".txt";
		readData();
	}
	
	public void readData() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		try {
			data  = Files.lines(Paths.get(inputFileName))
				.map(s -> s.split("\t\t"))
				.map(s -> new MonitoredData(LocalDateTime.parse(s[0], dtf), LocalDateTime.parse(s[1], dtf), s[2]))
				.collect(Collectors.toList());
		} catch (Exception e) {
			
		}
	}
	
	public void mapActivitiesByCount() {
		Map<String, Long> activities = data.stream()
				.map(d -> d.activityLabel)
				.collect(Collectors.groupingBy(d -> d, Collectors.counting()));
		activities.forEach((s, l) -> System.out.println("activity: " + s + ", count: " + l));
	}
	
	public void activitesPerDay() {
		Map<LocalDate, Map<String, Long>> activities = data.stream()
				.collect(Collectors.groupingBy(d -> d.startTime.toLocalDate(), Collectors.groupingBy(d -> d.activityLabel, Collectors.counting())));
		activities.forEach((d, m) -> System.out.println("Date: " + d + " " + m));
	}
	
	public void countDays() {
		System.out.println(data.stream().flatMap(d -> Stream.of(d.startTime, d.endTime)).map(d -> d.toLocalDate()).distinct().count());
	}
	
	public void showDuration() {
		data.stream().forEach(d -> System.out.println("activity: " + d.activityLabel +", duration: " 
				+ Duration.between(d.startTime, d.endTime).toHours() + ":"
				+ (Duration.between(d.startTime, d.endTime).toMinutes() - 60 * Duration.between(d.startTime, d.endTime).toHours())
				+ ":" + (Duration.between(d.startTime, d.endTime).getSeconds() - 60 * Duration.between(d.startTime, d.endTime).toMinutes())));
	}
	
	public void showTotalDuration() {
		Map<String, Integer> activityDuration = data.stream()
				.map(d -> d.activityLabel).distinct()
				.collect(Collectors.toMap(t -> t, t -> 1, Integer::sum));
		data.stream().forEach(d -> activityDuration.put(d.activityLabel, 0));
		data.stream().forEach(d -> activityDuration.put(d.activityLabel 
				, activityDuration.get(d.activityLabel) + (int) Duration.between(d.startTime, d.endTime).getSeconds()));
		activityDuration.forEach((s, i) -> System.out.println("activity: " + s + ", duration: " + intToTimeFormat(i)));
	}
	
	public void activitiesFilter() {
		data.stream().filter(d -> data.stream()
				.filter(d1 -> d1.activityLabel.equals(d.activityLabel))
				.filter(d1 -> Duration.between(d1.startTime, d1.endTime).getSeconds() < 300).count() >= 0.9 * data.stream()
				.filter(d2 -> d2.activityLabel.equals(d.activityLabel)).distinct().count())
		.map(d -> d.activityLabel).distinct().forEach(d -> System.out.println(d));
	}
	
	public void showData() {
		data.stream().forEach(d -> System.out.println(d));
	}
	
	private String intToTimeFormat(int time) {
		int hr = time / 3600;
		int min = time / 60 - hr * 60;
		int sec = time - min * 60 - hr * 3600;
		String hrStr = "";
		String minStr = "";
		String secStr = "";
		
		if(hr / 10 == 0) {
			hrStr = "0" + hr;
		} else
			hrStr = Integer.toString(hr);
		if(min / 10 == 0) {
			minStr = "0" + min;
		} else
			minStr = Integer.toString(min);
		if(sec / 10 == 0) {
			secStr = "0" + sec;
		} else
			secStr = Integer.toString(sec);
		return hrStr + ":" + minStr + ":" + secStr;
	}
}
