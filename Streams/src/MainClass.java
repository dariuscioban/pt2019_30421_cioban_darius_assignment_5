import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		DataProcessing dp = new DataProcessing("activities");
		Scanner keyboard = new Scanner(System.in);
		while(true) {
			System.out.println("Enter an integer (0 to stop): ");
			int option = keyboard.nextInt();
			clearScreen();
			if(option == 0)
				break;
			switch(option) {
				case 1: dp.countDays();;
						break;
				case 2: dp.mapActivitiesByCount();;
						break;
				case 3: dp.activitesPerDay();;
						break;
				case 4: dp.showDuration();
						break;
				case 5: dp.showTotalDuration();;
						break;
				case 6: dp.activitiesFilter();
						break;
	            default: System.out.println("Invalid command");;
	                     break;
			}
		}
		System.out.println("Terminated");
		keyboard.close();
	}
	
	public static void clearScreen() {  
		for (int i = 0; i < 2; ++i) 
			System.out.println();
	}  

}
